const AdmZip = require("adm-zip");
const args = process.argv.slice(2);
const zip = new AdmZip(`./data/${args[0]}`);
const templateName = args[0].slice(0, -4).replace(/\_/g, ":");
const accEntry = zip.getEntry(`${templateName}/accelerometer.csv`);
const gyroEntry = zip.getEntry(`${templateName}/gyro.csv`);
const Input = require("./model/Input");

function checkAccelerometerFile(accEntry) {
  if (accEntry == null) {
    return false;
  } else {
    return true;
  }
}
function checkGyroFile(gyroEntry) {
  if (gyroEntry == null) {
    return false;
  } else {
    return true;
  }
}
function saveAccelerometerToList(list) {
  let accList = [];
  for (let i = 1; i < list.length; i++) {
    let line = list[i].split(",");

    let temp_line = [];
    for (j = 1; j < line.length; j = j + 2)
      temp_line.push(line.slice(j, j + 2).join(","));

    accList.push(new Input(temp_line[0], temp_line[1], temp_line[2]));
  }
  return accList;
}

function saveGyroToList(list) {
  let gyroList = [];
  for (let i = 1; i < list.length; i++) {
    let line = list[i].split(",");

    let temp_line = [];
    for (j = 1; j < line.length; j = j + 2)
      temp_line.push(line.slice(j, j + 2).join(","));

    gyroList.push(new Input(temp_line[0], temp_line[1], temp_line[2]));
  }
  return gyroList;
}

let x = "";
let y = "";
if (checkAccelerometerFile(accEntry) && checkGyroFile(gyroEntry)) {
  console.log("It working");
  x = zip.readAsText(accEntry).split("\n");

  y = zip.readAsText(gyroEntry).split("\n");

  let accList = saveAccelerometerToList(x);
  let gyroList = saveGyroToList(y);
  console.log("Accelerometer List");
  console.log(accList);
  console.log("Gyroscope List");
  console.log(gyroList);
} else {
  console.log("Sprawdź poprawnośc plików w archiwum");
}
